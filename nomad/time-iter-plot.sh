#!/bin/bash

# First, determine largest subset for which they all converge
# Get f and k for each one

conv=time-iter-output/converged

if [ ! -f $conv ]; then
  list=($(ls time-iter-output/*.list))
  [ -z "$list" ] && echo "ERROR: missing list in time-iter-output" && exit 1
  [ ${#list[@]} -gt 1 ] && echo "ERROR: More than 1 list on time-iter-output" && \
    exit 1
  list=${list[0]}
  cp $list $conv

  for dir in time-iter-output/output-*
  do
    grep "0$" $dir/results | cut -f1 -d" " | sort $conv - | uniq -d > /tmp/tmp
    mv /tmp/tmp $conv
  done
else
  echo "$conv already exists. Skipping"
fi

for dir in time-iter-output/output-*
do
  name=$(basename $dir | cut -f2-3 -d-)
  for c in $(cat $conv)
  do
    grep -w $c $dir/results
  done | awk -v name=$name 'BEGIN{t=0; k=0; n=0};\
    NR > 1 { t += $4; k += $6; n += 1}; \
    { if ($7 != 0) print "ERRO", name, $1}; \
    END{print t, k, name}'
done > time-iter.data
#julia --precompiled=yes --depwarn=no time-iter.jl
