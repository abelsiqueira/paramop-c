#!/usr/bin/env python3

import sys

def compare_lines(lines1, lines2):
    N = len(lines1)
    diff = 0.0
    for i in range(1,N):
        s1 = lines1[i].split(); s2 = lines2[i].split()
        t1 = float(s1[3]); t2 = float(s2[3])
        c1 = int(s1[6]); c2 = int(s2[6]);
        k1 = float(s1[5]); k2 = float(s2[5])
        if c1 == 0 and c2 == 0:
            diff += t2 - t1
            if t1 < t2:
                print(s1[0], "Alg1 is better by", t2-t1)
            else:
                print(s1[0], "Alg2 is better by", t1-t2)
    print("Total difference is", diff)

with open(sys.argv[1], 'r') as f1:
    with open(sys.argv[2], 'r') as f2:
        compare_lines(f1.readlines(), f2.readlines())

