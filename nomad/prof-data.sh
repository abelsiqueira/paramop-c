#!/bin/bash

#targets=(1000-0 1000-1000)
targets=(1000-0)
path=time-iter-output/output
#cmd=echo
cmd="perprof --table"

# First, there must be a prof.data
if [ ! -f prof.data ]; then
  echo "Creating prof.data, this may take a while"
  # Creating prof files from result
  for d in time-iter-output/output*
  do
    ./execs/result-to-prof-table.sh $d/results > $d/prof
  done
fi

arg=""
for tgt in ${targets[@]}
do
  arg="$arg $path-$tgt/prof"
done

for d in time-iter-output/output-*
do
  name=$(basename $d | cut -d- -f2,3)
  found=0
  for tgt in ${targets[@]}; do if [ $tgt == $name ]; then found=1; break; fi; done
  [ $found == "1" ] && continue
  profout=$($cmd $arg $path-$name/prof | grep "\-$name/")
  rob=$(echo $profout | cut -f2 -d'|'); rob=$(echo ${rob:1:-2})
  eff=$(echo $profout | cut -f3 -d'|'); eff=$(echo ${eff:1:-1})
  echo $rob $eff $name
done > prof.data
