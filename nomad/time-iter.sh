#!/bin/bash

# This `runlist.sh` for every point in a grid

[ -z "$1" ] && mthdir=.. || mthdir=$1
[ ! -d $mthdir ] && echo "$mthdir is not a valid dir" && exit 1
[ ! -d $mthdir/cutest ] && echo "Could not find $mthdir/cutest" && exit 1

optfile=$mthdir/cutest/cutest.opt
maxiter=1000
maxtime=10

list=lists/cutest-lt10good.list
N=50
S=$((1000/N))

outdir=time-iter-output
rm -rf $outdir
mkdir -p $outdir
cp $list $outdir

for alpha in $(seq 1 $N)
do
  alpha=$(awk -v a=$alpha -v S=$S 'BEGIN{print S*a}')
  for beta in $(seq 0 $N)
  do
    beta=$(awk -v b=$beta -v S=$S 'BEGIN{print S*b}')
    awk -v a=$alpha -v b=$beta 'BEGIN{print "alpha", a/1000, "\nbeta", b/1000 }' \
      > $optfile
    echo "Running for $alpha-$beta"
    ./execs/runlist.sh $list
    mv $(readlink last) $outdir/output-$alpha-$beta
  done
done
