#!/bin/bash

[ $# -lt 1 ] && echo "Need file" && exit 1
file=$1

[ ! -f $file ] && echo "$file is not a file" && exit 1

if [ "$2" == "--use-iter" ]; then
  echo -e "---\nalgname: $file\nsuccess: \"0\"\nfree_format: True\ncol_time: 6\nmintime: 1\n---"
elif [ $# -eq 1 -o "$2" == "--use-time" ]; then
  echo -e "---\nalgname: $file\nsuccess: \"0\"\nfree_format: True\n---"
else
  echo "ERROR: Argument $2 not recognized" && exit 1
fi
awk ' NR >= 2{
  printf "%-9s %s %12.6e %15s %15s %d\n", $1, $7, $4, $2, $3, $6
}' $file
