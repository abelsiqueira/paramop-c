#!/bin/bash

# This is for NOMAD. This file is called as
#     ./objfun6.sh ALPHA BETA ETA1 ETA2 SIGMA1 SIGMA2
# where ARGS are 6 options for the method.
# We write these arguments to a file cutest.opt, and run all
# cutest problem of a specified file to it.

tol="1e-5"
kmax=1000
maxtime=30

method_dir=../..

function usage {
  echo "Usage:\n  $0 x.txt"
  echo "where x.txt has a line of the form"
  echo "ALPHA*1000 BETA*1000 ETA1*1000 ETA2*100 SIGMA1*100 SIGMA2*10"
}

[ $# -lt 1 ] && echo "ERROR: Need x.txt" && usage && exit 1

[ ! -f nomad.iter ] && echo "Run nomad-run.sh" && exit 1
[ ! -f nomad.dir ] && echo "Run nomad-run.sh" && exit 1
nomaddir=$(cat nomad.dir)
[ ! -d "$nomaddir" ] && echo "Run nomad-run.sh" && exit 1

args=(alpha beta eta1 eta2 sigma1 sigma2)
scaling=(1000 1000 1000 100 100 10)
params=($(cat $1))
optfile=$method_dir/cutest/cutest.opt

# eta1 < eta2
cons=$(echo | awk -v n1=${params[2]} -v n2=${params[3]} \
  -v s1=${scaling[2]} -v s2=${scaling[3]} \
  '{print (n1/s1 < n2/s2 ? 0 : 1)}')
if [ $cons -eq "1" ];
then
  echo "123456789 1"
  exit
fi

for ((i = 0; i < ${#args[@]}; i++))
do
  echo | awk -v a1=${args[$i]} -v n1=${params[$i]} -v s1=${scaling[$i]} \
    '{print a1, n1/s1}'
done > $optfile

cat >> $optfile << EOF
tol $tol
kmax $kmax
maxtime $maxtime
EOF

#list=cutest-lt10good.list
list=cutest-lt500var.list
#list=cutest-test.list
#list=cutest-2var-free.list

./runlist.sh $list $method_dir > log

# Saving the results
iter=$(cat nomad.iter)
cp last/results $nomaddir/$iter-results
x=$(echo $iter | sed 's/^0*//')
awk "BEGIN {printf \"%0${#iter}d\", $x+1;exit}" > nomad.iter

readlink last >> log

f=$(awk -v t=$maxtime 'BEGIN{ f = 0 }
      NR >= 2 { f = f + $4; }
    END{ printf "%15.8e\n 0", f }' last/results)
echo $f >> log

mv $(readlink last) $nomaddir/$iter-cutest-dir
echo $f
