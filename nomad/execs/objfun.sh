#!/bin/bash

# This is for NOMAD. This file is called as
#     ./objfun.sh ARGS
# where ARGS are 7 (or more, later) options for the method.
# We write these arguments to a file cutest.opt, and run all
# cutest problem of a specified file to it.

tol="1e-5"
kmax=1000
maxtime=30

function usage {
  echo "Usage:\n  ./objfun.sh x.txt"
  echo "where x.txt has a line of the form"
  echo "ALPHA*1000 BETA*1000 ETA1*1000 ETA2*100 SIGMA1*100 SIGMA2*10"
  #echo "ETA1*1000 ETA2*100 SIGMA1*100 SIGMA2*10"
  #echo "ALPHA BETA ETA1 ETA2 INIT_CONV_MONO SIGMA1 SIGMA2"
  #echo "ALPHA BETA"
}

[ $# -lt 1 ] && echo "ERROR: Need x.txt" && usage && exit 1
if [ $# -gt 1 ]; then
  if [ $2 == "standalone" ]; then
    standalone=yes
  else
    echo "ERROR: Don't know what is '$2'"
    exit 1
  fi
fi

if [ -z "$standalone" ]; then
  [ ! -f nomad.iter ] && echo "Run nomad-run.sh" && exit 1
  [ ! -f nomad.dir ] && echo "Run nomad-run.sh" && exit 1
  nomaddir=$(cat nomad.dir)
  [ ! -d "$nomaddir" ] && echo "Run nomad-run.sh" && exit 1
fi

#args=(alpha beta eta1 eta2 sigma1 sigma2)
args=(eta1 eta2 sigma1 sigma2)
scaling=(1000 100 100 10)
#args=(alpha beta eta1 eta2 init_conv_mono sigma1 sigma2)
#scaling=(1000 1000 1000 100 100 10)
#args=(alpha beta)
#scaling=(1000 1000)
params=($(cat $1))
optfile=../cutest/cutest.opt

# eta1 < eta2
#cons=$(echo | awk -v n1=${params[0]} -v n2=${params[1]} \
#  -v s1=${scaling[0]} -v s2=${scaling[1]} \
#  '{print (n1/s1 < n2/s2 ? 0 : 1)}')
#if [ $cons -eq "1" ];
#then
#  echo "1e20 1"
#  exit
#fi

for ((i = 0; i < ${#args[@]}; i++))
do
  echo | awk -v a1=${args[$i]} -v n1=${params[$i]} -v s1=${scaling[$i]} \
    '{print a1, n1/s1}'
done > $optfile

#eta1 0.200504
#eta2 0.849801
#sigma1 0.699780
#sigma2 2.44949
#alpha 0.516376
#beta 0.00113
cat >> $optfile << EOF
tol $tol
kmax $kmax
maxtime $maxtime
EOF

#list=cutest-test.list
#list=audet.list
#list=cutest-lt10good.list
#list=cutest-lt100var.list
list=cutest-lt500good.list
#list=cutest-lt500select.list
#list=cutest-lt10var.list
#list=cutest-test.list
./runlist.sh $list > /dev/null

# Saving the results
if [ -z "$standalone" ]; then
  iter=$(cat nomad.iter)
  cp last/results $nomaddir/$iter-results
  x=$(echo $iter | sed 's/^0*//')
  awk "BEGIN {printf \"%0${#iter}d\", $x+1;exit}" > nomad.iter
fi

awk -v t=$maxtime 'BEGIN{ f = 0 }
      NR >= 2 { f = f + $4; }
      END{ printf "%15.8e\n 0", f }' last/results
#awk -v t=$maxtime 'BEGIN{ f = 0 }
#      NR >= 2 {if ($6 == "0")
#        f = f + $4
#      else
#        f = f + t;}
#      END{ printf "%15.8e\n 0", f }' last/results

if [ -z "$standalone" ]; then
  mv $(readlink last) $nomaddir/$iter-cutest-dir
fi
