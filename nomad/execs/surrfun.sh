#!/bin/bash

tol="1e-5"
kmax=1000
maxtime=30

function usage {
  echo "Usage:\n  ./objfun.sh x.txt"
}

[ $# -lt 1 ] && echo "ERROR: Need x.txt" && usage && exit 1
if [ $# -gt 1 ]; then
  if [ $2 == "standalone" ]; then
    standalone=yes
  else
    echo "ERROR: Don't know what is '$2'"
    exit 1
  fi
fi

if [ -z "$standalone" ]; then
  [ ! -f nomad.iter ] && echo "Run nomad-run.sh" && exit 1
  [ ! -f nomad.dir ] && echo "Run nomad-run.sh" && exit 1
  nomaddir=$(cat nomad.dir)
  [ ! -d "$nomaddir" ] && echo "Run nomad-run.sh" && exit 1
fi

args=(alpha beta eta1 eta2 sigma1 sigma2)
#args=(eta1 eta2 sigma1 sigma2)
#scaling=(1000 100 100 10)
#args=(alpha beta eta1 eta2 init_conv_mono sigma1 sigma2)
#scaling=(1000 1000 1000 100 100 10)
#args=(alpha beta)
#scaling=(1000 1000)
params=($(cat $1))
optfile=../cutest/cutest.opt

# eta1 < eta2
cons=$(echo | awk -v n1=${params[2]} -v n2=${params[3]} \
  '{print (n1 < n2 ? 0 : 1)}')
if [ $cons -eq "1" ];
then
  echo "1e20 1"
  exit
fi

for ((i = 0; i < ${#args[@]}; i++))
do
  echo | awk -v a1=${args[$i]} -v n1=${params[$i]} '{print a1, n1}'
done > $optfile

#eta1 0.200504
#eta2 0.849801
#sigma1 0.699780
#sigma2 2.44949
#alpha 0.516376
#beta 0.00113
cat >> $optfile << EOF
tol $tol
kmax $kmax
maxtime $maxtime
EOF

list=cutest-lt10good.list
./runlist.sh $list > /dev/null

# Saving the results
if [ -z "$standalone" ]; then
  iter=$(cat nomad.iter)
  cp last/results $nomaddir/$iter-results
  x=$(echo $iter | sed 's/^0*//')
  awk "BEGIN {printf \"%0${#iter}d\", $x+1;exit}" > nomad.iter
fi

awk -v t=$maxtime 'BEGIN{ f = 0 }
      NR >= 2 { f = f + $4; }
      END{ printf "%15.8e\n 0", f }' last/results

if [ -z "$standalone" ]; then
  mv $(readlink last) $nomaddir/$iter-cutest-dir
fi
