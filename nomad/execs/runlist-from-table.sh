#!/bin/bash

# Run ./objfun.sh for each line on a table file generated from
# report.sh

# Example:
# ./runlist-from-table.sh report.table ../../ cutest-unc-test.list FOLDER-IF-CONTINUING

set -e

[ $# -lt 3 ] && echo "Usage: $0 INPUT METHOD_ROOT LIST [DIR]" && exit 1
input=$1
[ -z "$input" ] && echo "Need file" && exit 1
method_dir=$(readlink -f $2)
[ ! -d $method_dir ] && echo "$method_dir is not the method root" && exit 1
list=$3
[ ! -f $list ] && echo "$list is not a file" && exit 1

optfile=$method_dir/cutest/cutest.opt

if [ $# -lt 4 ]; then
  dir=table-run-$(date +"%Y-%m-%d-%H-%M-%S")
  mkdir -p $dir
else
  dir=$4
  if [ ! -d "$dir" ]; then
    mkdir $dir
  else
    direxits=1
  fi
fi

file=table-result

cp $input $dir
cp $list $dir
cd $dir

if [ -z "$direxits" ]; then
  for sh in runlist results obj-from-result result-to-prof-table
  do
    ln -s $method_dir/nomad/${sh}.sh .
  done
fi

args=(alpha beta eta1 eta2 sigma1 sigma2)
scaling=(1000 1000 1000 100 100 10)
k=1

awk 'NR > 8 {print $0}' $input | while read line
do
  x=($(echo $line | sed 's/.*(\(.*\)).*/\1/g'))
  aux=$(echo | awk "{printf \"%4.2f-%4.2f\", ${x[0]}/1000, ${x[1]}/1000}")

  if [ -d output-$aux ]; then
    echo "Line number $k already done: $aux"
    k=$((k+1))
    continue
  else
    echo "Running line number $k - $aux"
    echo "Running from" >> $file
  fi

  cat > $optfile << EOF
tol 1e-5
kmax 10000
maxtime 3600
EOF

  for i in $(seq 0 $((${#args[@]}-1)) )
  do
    echo "  ${args[$i]} = ${x[$i]}"
    echo | awk  -v arg=${args[$i]} -v val=${x[$i]} -v s=${scaling[$i]} \
      '{ printf "%s %s\n", arg, val/s }' >> $optfile
  done >> $file
  ./runlist.sh $list $method_dir > /dev/null 
  echo "  f = $(./obj-from-result.sh last/results)" >> $file

  k=$((k+1))

  mv $(readlink last) output-$aux
  cd output-$aux
  ln -s ../result-to-prof-table.sh
  ./result-to-prof-table.sh
  cd ..
  cp output-$aux/output-*.table .
done
