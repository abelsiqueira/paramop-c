#!/bin/bash

output=$(mktemp)
for file in nomad-result-*.out
do
  line=$(tail -n 1 $file)
  f=$(echo $line | cut -f3 -d'=')
  x=$(echo $line | sed 's/.*(\(.*\)).*/\1/')
  echo "$f ($x)" >> $output
done
sort -g $output
