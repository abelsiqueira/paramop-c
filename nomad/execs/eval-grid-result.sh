#!/bin/bash

if [ $# -lt 1 ]; then
  output=$(mktemp)
  for file in */nomad_result
  do
    while read line
    do
      line=$(echo $line | sed 's/+\s\+//g') # Workaround for (VNS) case
      f=$(echo $line | awk '{print $2}')
      x=$(echo $line | sed 's/.*(\([0-9 \.]*\)).*/\1/')
      echo "$f ($x)" >> $output
    done < $file
  done
  sort -g $output
else
  file=$1
  [ ! -f $file ] && echo "$1 is not a nomad_result file" && exit 1
  output=$(mktemp)
  while read line
  do
    line=$(echo $line | sed 's/+\s\+//g') # Workaround for (VNS) case
    f=$(echo $line | awk '{print $2}')
    x=$(echo $line | sed 's/.*(\([0-9 \.]*\)).*/\1/')
    echo "$f ($x)" >> $output
  done < $file
  sort -g $output
fi
