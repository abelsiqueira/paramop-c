#!/bin/bash

[ $# -lt 1 ] && echo "Needs arguments" && exit 1
dir=$1
[ ! -d "$dir" ] && echo "$dir is not a dir" && exit 1

for result in $(find -L $dir -name "*-results")
do
  awk -v t=30 'BEGIN{ f = 0; d = 0 }
      NR >= 2 {
      f = f+ $4
      if ($6 != "0")
        d = d + 1;}
      END{
        printf "Sum of time: %14f, ", f;
        printf "Failures: %5d\n", d}' $result
done
