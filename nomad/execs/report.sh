#!/bin/bash

# This generates the report for the grid run

echo "Hello"
echo "The best 5 objetive functions found are"
head -n 5 report
echo "For each initial point, NOMAD found the best point to be"
for dir in nomad-*
do
  alpha=$(echo $dir | cut -f2 -d"-")
  beta=$(echo $dir | cut -f3 -d"-")
  sol=$(cat nomad-$alpha-$beta/nomad_sol | tr '\n' ' ')
  f=$(./eval-grid-result.sh $dir/nomad_result | head -1 | cut -d" " -f1)
  #sol=$(echo $sol | cut -d" " -f 1-2)
  #sol=$(echo $sol | sed 's/ /\/1000,/')
  printf "%4s %4s -> %s (%s)\n" $alpha $beta $f "$sol"
done
