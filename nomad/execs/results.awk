BEGIN{ f = 0 }
NR >= 2 {
  if ($6 == "0")
    f = f + $4
  else
    f = f + 10;
}
END{ printf "%15.8e\n 0", f }
