#!/bin/bash

# This run a list of problem of CUTEst.

function usage {
  echo "Usage:\n  ./runlist.sh LIST"
}

[ $# -lt 1 ] && usage && exit 1
list=$1
[ ! -f $list ] && echo "ERROR: $list is not a file" && usage && exit 1

if [ $# -lt 2 ]; then
  method_dir=..
else
  [ ! -d $2 ] && echo "$2 is not a valid folder" && exit 1
  method_dir=$2
fi
[ ! -d $method_dir/cutest ] && echo "cutest folder not found in $method_dir" \
  && exit 1
[ ! -f $method_dir/Makefile ] && echo "Makefile not found in $method_dir" \
  && exit 1

rm -f last
dir=cutest-$(date +"%Y-%m-%d_%H:%M")
n=0
while [ -d $dir ]; do
  n=$((n+1))
  dir=cutest-$(date +"%Y-%m-%d_%H:%M")_$n
done
mkdir -p $dir
ln -s $dir last
cp $1 $dir
cp $method_dir/cutest/cutest.opt $dir

k=1
T=$(wc -l $1 | cut -f1 -d" ")

for prob in $(cat $list)
do
  printf "Running problem %4d of %4d: $prob\n" $k $T
  make -C $method_dir cutest PROBLEM=$prob > $dir/$prob.out
  k=$((k+1))
done

$method_dir/nomad/execs/results.sh $dir > $dir/results
