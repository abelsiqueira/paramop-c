#!/bin/bash

[ -z "$1" ] && echo "Need paramop.txt nomad file" && exit 1
nomad_file=$1
[ -z "$2" ] && echo "Need method dir" && exit 1
[ ! -d $2 ] && echo "$2 is not a dir" && exit 1
[ ! -d $2/cutest ] && echo "$2/cutest doesn't exist" && exit 1

maxiter=$(awk '/MAX_BB_EVAL/ {print $2}' $nomad_file)
printf "%0${#maxiter}d" 0 > nomad.iter
dir=nomad-$(date +"%Y-%m-%d_%H:%M")
mkdir $dir
echo $dir > nomad.dir

nomad $nomad_file
cp nomad_result.0.txt $dir/nomad_result
cp nomad_sol.0.txt $dir/nomad_sol
#rm -f nomad.iter nomad.dir
