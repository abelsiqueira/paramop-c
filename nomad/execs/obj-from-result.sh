#!/bin/bash

[ $# -lt 1 ] && echo "Need result file" && exit 1
[ ! -f $1 ] && echo "$1 is not a file" && exit 1

awk -v t=30 'BEGIN{f=0} NR >= 2 { f = f + $4; } END{ printf "%15.8e\n", f }' $1
