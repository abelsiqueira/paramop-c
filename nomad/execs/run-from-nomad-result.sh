#!/bin/bash

# Run ./objfun.sh for each line on a file like
# nomad-grid-100nomadeval.result

input=$1
[ -z "$input" ] && echo "Need file" && exit 1

dir=tmp
mkdir -p $dir
file=$dir/reruns-from-grid
rm -f $file

k=1
while read line
do
  f=$(echo $line | cut -f1 -d" ")
  x=$(echo $line | sed 's/.*(\(.*\)).*/\1/g')
  echo "$k, f = $f"
  echo $x > x.txt
  echo "Line $k, f = $f" >> $file
  echo "  x = ( $x )" >> $file
  ./objfun.sh x.txt >> $file
  k=$(($k+1))
done < $input

awk 'NR%3{printf "%s ",$0;next;}1' $file | sed 's/^ 0//g' | \
  sort -k17 -g > $file-sorted
