#!/bin/bash

# Extract the important part of a OUT file

function sum {
  awk "BEGIN{print $1+$2; exit}"
}

if [ -d "$1" ]; then
  args="name fx |gx| otime etime k flag"
  printf "%-8s %15s %15s %15s %15s %7s %1s\n" $args
  for file in $(ls $1/*.out)
  do
    fx=$(awk '/fx/ {print $3}' $file)
    ngx=$(awk '/gx/ {print $3}' $file)
    ot=$(awk '/o_time/ {print $3}' $file)
    et=$(awk '/elapsed/ {print $4}' $file)
    k=$(awk '/k = / {print $3}' $file)
    ef=$(awk '/exit/ {print $4}' $file)
    prob=${file//$1\//}
    args="${prob//.out/} $fx $ngx $ot $et $k $ef"
    printf "%-8s %15s %15s %15s %15s %7s %1s\n" $args
  done
  exit 0
fi
[ ! -f "$1" ] && echo "ERROR" && exit 1

grep "fx" $1
grep "gx" $1
grep "k = " $1
grep "exit flag" $1
grep "elapsed time" $1
