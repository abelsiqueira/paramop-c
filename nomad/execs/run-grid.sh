#!/bin/bash

d=$(date +"%Y-%m-%d_%H%M")
#file=grid-$d.result
file=grid-$d
mkdir $file-dir
grep "^list=" objfun.sh > $file
# Alpha = 0.25 0.5 0.75 1.0
for alpha in $(seq 1 4)
do
  alpha=$(awk -v a=$alpha 'BEGIN{print 250*a}')
  # Beta = 0.0 0.25 0.5 0.75 1.0
  for beta in $(seq 0 4)
  do
    beta=$(awk -v b=$beta 'BEGIN{print 250*b}')
    echo "Running $alpha $beta"
    #echo "$alpha $beta" > x.txt
    #f=$(./objfun.sh x.txt standalone | head -n1)
    echo "($alpha $beta) $f" >> $file
    sed "s/ALPHA/$alpha/g" paramop-template.txt | \
      sed "s/BETA/$beta/g" > paramop.txt
    ./nomad-run.sh paramop.txt > nomad-result-$alpha-$beta.out
    ln -s "../$(cat nomad.dir)" $file-dir/nomad-$alpha-$beta
  done
done

ln -s ../eval-grid-result.sh $file-dir
ln -s ../report.sh $file-dir
