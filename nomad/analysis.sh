#!/bin/bash

# Table of results optimizing


function feval() {
  tail -n 1 $1 | cut -d'=' -f3
}

#profcmd="perprof --tikz -f --semilog"
profcmd="echo"

cd article-run

cat << EOF > table4.tex
\\begin{tabular}{|l|l||l|l|l|l|l|} \\hline
\$\\alpha\$ & \$\\beta\$ & \$f\$ & \$\\eta_1\$ & \$\\eta_2\$ & \$\\sigma_1\$ &
\$\\sigma_2\$ \\\\ \\hline
EOF

for dir in grid-dir/nomad-m-*
do
  alpha=$(basename $dir | cut -f3 -d-)
  beta=$(basename $dir | cut -f4 -d-)
  sol=$(cat $dir/nomad_sol | tr '\n' ' ')
  f=$(feval grid-dir/nomad-result-m-$alpha-$beta.out)
  echo $alpha $beta $f $sol
done | awk '{printf "%.3f & %.3f & %7f & %.6f & %.6f & %.6f & %.6f \\\\ \
\\hline\n", $1/1000, $2/1000, $3, $4/1000, $5/100, $6/100, $7/10}' >> table4.tex

cat << EOF >> table4.tex
\\end{tabular}
EOF

cat << EOF > table6.tex
\\begin{tabular}{|l|l||l|l|l|l|l|l|l|} \\hline
\$\\alpha_0\$ & \$\\beta_0\$ & \$f\$ & \$\\alpha^*\$ & \$\\beta^*\$ &
\$\\eta_1\$ & \$\\eta_2\$ & \$\\sigma_1\$ & \$\\sigma_2\$ \\\\ \\hline
EOF


for dir in grid-dir/nomad-[0-9]*
do
  alpha=$(basename $dir | cut -f2 -d-)
  beta=$(basename $dir | cut -f3 -d-)
  sol=$(cat $dir/nomad_sol | tr '\n' ' ')
  f=$(feval grid-dir/nomad-result-$alpha-$beta.out)
  echo $alpha $beta $f $sol
done | awk '{printf "%.3f & %.3f & %7f & %.3f & %.3f & %.6f & %.6f & %.6f & %.6f \\\\ \
\\hline\n", $1/1000, $2/1000, $3, $4/1000, $5/1000, $6/1000, $7/100, $8/100, \
$9/10}' >> table6.tex

cat << EOF >> table6.tex
\\end{tabular}
EOF

cat << EOF > improvement.tex
\\begin{tabular}{|l|l||l|l|}
\$\\alpha\$ & \$\\beta\$ & \$f_{\\mbox{old}}\$ & \$f\$ \\\\ \\hline
EOF

for dir in grid-dir/nomad-[0-9]*
do
  alpha=$(basename $dir | cut -f2 -d-)
  beta=$(basename $dir | cut -f3 -d-)
  f=$(feval grid-dir/nomad-result-$alpha-$beta.out)
  fold=$(feval grid-dir/nomad-result-m-$alpha-$beta.out)
  echo $alpha $beta $fold $f
done | awk '{printf "%.3f & %.3f & %11.5f & %11.5f \\\\ \\hline\n", $1/1000, \
$2/1000, $3, $4}' >> improvement.tex

cat << EOF >> improvement.tex
\\end{tabular}
EOF

for f in result-[01]*
do
  alpha=$(echo $f | cut -f2 -d-)
  beta=$(echo $f | cut -f3 -d-)
  sp=$(echo $f | cut -f4 -d-)
  name="\$\\\\alpha = $alpha, \\\\beta = $beta\$ $sp"
  [ ! -z "$sp" ] && sp="-sp"
  file=prof-$alpha-$beta$sp.table
  echo -e "---\nalgname: $name\nsuccess: \"0\"\nfree_format: True\n---" > $file
  awk 'NR >= 2 {printf "%-9s %s %12.6e %15s %15s\n", $1, $7, $4, $2, $3}' \
    $f/results >> $file
done

perprof prof*.table --table -o all-rob-eff

mkdir -p profs
files=($(ls prof*.table))
n=${#files[@]}
for i in $(seq 0 $((n-1)))
do
  for j in $(seq $((i+1)) $((n-1)))
  do
    ni=$(echo ${files[$i]} | sed 's/prof-\(.*\).table/\1/g')
    nj=$(echo ${files[$j]} | sed 's/prof-\(.*\).table/\1/g')
    $profcmd ${files[$i]} ${files[$j]} -o profs/$ni-vs-$nj
  done
done

sort -gr -k8 all-rob-eff.tex | head -3 | while read line
do
  alpha=$(echo $line | cut -f3 -d" " | cut -f1 -d,)
  beta=$(echo $line | cut -f6 -d" " | cut -f1 -d$)
  echo prof-$alpha-$beta.table
done | xargs $profcmd -o profs/top-rob

sort -gr -k11 all-rob-eff.tex | head -3 | while read line
do
  alpha=$(echo $line | cut -f3 -d" " | cut -f1 -d,)
  beta=$(echo $line | cut -f6 -d" " | cut -f1 -d$)
  echo prof-$alpha-$beta.table
done | xargs $profcmd -o profs/top-eff

tar -cf param-report-plots.tar *.tex profs/*.pdf
cd ..
mv article-run/param-report-plots.tar .
gzip param-report-plots.tar
