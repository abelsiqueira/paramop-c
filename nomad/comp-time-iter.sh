#!/bin/bash

if [ $# -lt 1 ]; then
  dir=time-iter-output
else
  dir=$1
  [ ! -d $1 ] && echo "$1 is not a dir" && exit 1
fi

printf "%5s %8s %8s\n" Conv Time TimeT

for d in $dir/output*
do
  printf "%-16s " $(basename $d)
  awk 'BEGIN{t = 0; tc = 0; c = 0; n = 0;};
    NR >= 2 { { if ($7 == "0") { tc += $4; c += 1} }; {t += $4; n += 1;}; }
    END{ printf "%2.2f %8.2e %8.2e\n", 100*c/n, tc, t}' $d/results
    #END{ print c/n, tc, t };' $d/results
done
