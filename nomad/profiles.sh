#!/bin/bash

# These are all the profiles needed to the article
# Hardcoded, due to time limitations

prefix=time-iter-output/output
rm -f profs/*
mkdir -p profs
subset=time-iter-output/converged

cmd="perprof --tikz -f --semilog --black-and-white --no-title"

# Best of time-iter / julia time-iter.jl
best_time=$(sort time-iter.data -gr -k1 | tail -n 1 | cut -f3 -d" ")
best_iter=$(sort time-iter.data -gr -k2 | tail -n 1 | cut -f3 -d" ")
best_robustness=$(sort prof.data -g -k1 | tail -n 1 | cut -f3 -d" ")
best_efficiency=$(sort prof.data -g -k2 | tail -n 1 | cut -f3 -d" ")

for t in time iter robustness efficiency
do
  echo -e "---\nalgname: \"Best of $t\"" > profs/$t.prof
  echo "col_time: 6" >> profs/$t.prof
  eval tail -n +3 $prefix-\$best_$t/prof >> profs/$t.prof
done

echo -e "---\nalgname: \"(1,0)\"" > profs/trad-1-0.prof
echo "col_time: 6" >> profs/trad-1-0.prof
tail -n +3 $prefix-1000-0/prof >> profs/trad-1-0.prof
echo -e "---\nalgname: \"(1,1)\"" > profs/trad-1-1.prof
echo "col_time: 6" >> profs/trad-1-1.prof
tail -n +3 $prefix-1000-1000/prof >> profs/trad-1-1.prof

arg=""
for t in time iter robustness efficiency
do
  f=profs/$t.prof
  arg="$arg $f"
  $cmd $f profs/trad-1-0.prof -o profs/trad-vs-$t
  $cmd $f profs/trad-1-0.prof -o profs/trad-vs-$t-subset --subset=$subset
done

$cmd $arg -o profs/best-all
$cmd $arg -o profs/best-all-subset --subset=$subset
$cmd profs/{time,iter}.prof -o profs/best-time-iter
$cmd profs/{time,iter}.prof -o profs/best-time-iter-subset --subset=$subset
$cmd profs/{robustness,efficiency}.prof -o profs/best-rob-eff
$cmd profs/{robustness,efficiency}.prof -o profs/best-rob-eff-subset --subset=$subset

$cmd profs/trad-1-{0,1}.prof -o profs/traditional
$cmd profs/trad-1-{0,1}.prof -o profs/traditional-subset --subset=$subset

rm -f profs/*.{aux,log,tex}
