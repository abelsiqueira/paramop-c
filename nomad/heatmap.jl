using Plots
pyplot(size=(600,600*3/4), reuse=true)
using LaTeXStrings

const bw = ColorGradient(vcat(:white, :black))
const bwinv = ColorGradient(vcat(:black, :white))

function camel(s)
  ss = split(s)
  if length(ss) == 1
    return uppercase(s[1:1]) * lowercase(s[2:end])
  else
    return join([camel(x) for x in ss])
  end
end

function image(α, β, A; name="tmp", colormap=bwinv, title="")
  heatmap(α, β, A', c=colormap)
  xlims!(minimum(α), maximum(α))
  ylims!(minimum(β), maximum(β))
  zlims!(floor(minimum(A)), ceil(maximum(A)))
  xlabel!(L"$\alpha$")
  ylabel!(L"$\beta$")
  #title!(title)
  png("$(name)-heatmap")
end

function area_percentage(A, lower_is_better; p = 0.1)
   n = length(A)
   selection(x) = lower_is_better ? sum(A .<= x)/n : sum(A .>= x)/n
   L = sort(A[:], rev=lower_is_better)
   for x = L
     println("s($x) = $(selection(x))")
     if selection(x) <= p
       M = maximum(A)
       m = minimum(A)
       λ = M-m
       q = lower_is_better ? (x-m)/λ : (M-x)/λ
       println("$x $(selection(x)) $m $q")
       return 1e-5/(M+m) + q
     end
   end
   return 0
end

function best(A, lower_is_better; p = 0.1, N = 3)
  M = maximum(A)
  m = minimum(A)
  λ = M-m
  if p < 0
    p = area_percentage(A, lower_is_better, p=-p)
  end
  println("m, M, p = $m, $M, $p")
  if lower_is_better
    x = m + p*λ
    I = find(A .< x)
    x = m + 1.1*p*λ
  else
    x = M - p*λ
    I = find(A .> x)
    x = M - 1.1*p*λ
  end
  J = setdiff(1:prod(size(A)), I)
  A[J] = x
  return A, I
end

function time_iter()
  α, β = [], []

  lines = readlines(open("time-iter.data", "r"))

  data = Dict()
  for line in lines
    sline = split(line)
    αi, βi = map(x->parse(Int, x), split(sline[3], "-"))
    t = parse(sline[1])
    k = parse(sline[2])
    data[(αi,βi)] = (t,k)
    push!(α, αi)
    push!(β, βi)
  end
  α = sort(unique(α))
  β = sort(unique(β))
  T = zeros(length(α), length(β))
  K = zeros(length(α), length(β))
  for (αi,βi) in keys(data)
    i, j = findfirst(α, αi), findfirst(β, βi)
    T[i,j], K[i,j] = data[(αi,βi)]
  end

  α, β = α/1000, β/1000
  N = length(α)
  for (A, name, low_is_bet, p) in [(T, "time", true, -0.1),
      (K, "iter", true, -0.1)]
    title = camel(name)
    for use_best in [false, true]
      if use_best
        A, I = best(A, low_is_bet, p=p)
        scatter(α[(I-1)%N+1], β[div(I-1,N)+1], zcolor=A[I], c=bwinv,
          m=(5,stroke(0)), leg=false)
        xlims!(-0.05, 1.05)
        ylims!(-0.05, 1.05)
        xlabel!(L"$\alpha$")
        ylabel!(L"$\beta$")
        png("$name-best-scatter")
        image(α, β, A, name="$name-best", title="Best of $title")
      else
        image(α, β, log(A), name=name, title=title)
      end
    end
  end
end

function comp()
  α, β = [], []

  lines = readlines(open("comp.data", "r"))
  shift!(lines)

  data = Dict()
  for line in lines
    sline = split(line)
    αi, βi = map(x->parse(Int, x), split(sline[1][8:end], "-"))
    r = parse(sline[2])
    e = parse(sline[3])
    es = parse(sline[4])

    data[(αi,βi)] = (r,e,es)
    push!(α, αi)
    push!(β, βi)
  end
  α = sort(unique(α))
  β = sort(unique(β))
  rob = zeros(length(α), length(β))
  eff = zeros(length(α), length(β))
  comb = zeros(length(α), length(β))
  for (αi,βi) in keys(data)
    i, j = findfirst(α, αi), findfirst(β, βi)
    rob[i,j], eff[i,j], comb[i,j] = data[(αi,βi)]
  end

  α, β = α/1000, β/1000
  N = length(α)
  for (A, name, low_is_bet, p) in [(rob, "robustness", false, -0.1),
      (eff, "efficiency", true, -0.1), (comb, "combination", true, -0.0001)]
      title = camel(name)
    for use_best in [false, true]
      g = low_is_bet ? bwinv : bw
      println(name)
      if use_best
        A, I = best(A, low_is_bet, p=p)
        scatter(α[(I-1)%N+1], β[div(I-1,N)+1], zcolor=A[I], c=g,
          m=(5,stroke(0)), leg=false)
        xlims!(-0.05, 1.05)
        ylims!(-0.05, 1.05)
        xlabel!(L"$\alpha$")
        ylabel!(L"$\beta$")
        png("$name-best-scatter")
        image(α, β, A, name="$name-best", colormap=g, title="Best of $title")
      else
        image(α, β, A, name=name, colormap=g, title=title)
      end
    end
  end

  SUrob = sort(unique(rob[:]))


  println("Max: $(maximum(SUrob))")
  conv_thresh = SUrob[end]
  Iconv = find(rob .== conv_thresh)
  scatter(α[(Iconv-1)%N+1], β[div(Iconv-1,N)+1],
    m=(5,:black,stroke(0)), lab="$conv_thresh %")

  conv_thresh = SUrob[end-1]
  Iconv = find(rob .== conv_thresh)
  scatter!(α[(Iconv-1)%N+1], β[div(Iconv-1,N)+1],
    m=(4,:blue,stroke(0)), lab="$conv_thresh %")

  conv_thresh = SUrob[end-2]
  Iconv = find(rob .== conv_thresh)
  scatter!(α[(Iconv-1)%N+1], β[div(Iconv-1,N)+1],
    m=(3,:red,stroke(0)), lab="$conv_thresh %")
  xlims!(-0.05, 1.25)
  ylims!(-0.05, 1.05)
  xlabel!(L"$\alpha$")
  ylabel!(L"$\beta$")
  png("robustness")

  for i = 0:2
    conv_thresh = SUrob[end-i]
    Iconv = find(rob .>= conv_thresh)
    m = minimum(eff[Iconv])
    λ = maximum(eff[Iconv])-m
    scatter(α[(Iconv-1)%length(α)+1], β[div(Iconv-1,length(β))+1], zcolor=rob[Iconv],
        c=:grays, ms=-8(eff[Iconv]-m)/λ+15, m=:grays)
    xlims!(minimum(α), maximum(α))
    ylims!(minimum(β), maximum(β))
    xlabel!(L"$\alpha$")
    ylabel!(L"$\beta$")
    #title!("Efficiency of most robust problems (at least $conv_thresh %)")
    png("conv$i")
  end
end

pyplot()
time_iter()
comp()
