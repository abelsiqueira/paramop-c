#!/bin/bash

# For each (α,β) in a grid
#  Run nomad for the 4 parameters (1)
#  Run nomad for the 6 parameters (2)
#  Run the 6 parameters found agains the complete set of problems (3)
# Then, we use the optimal values for (α=1,β=0) and (α=1,β=1) from (1) and
# run the complete set of problems in (3).

d=$(date +"%Y-%m-%d_%H%M")
file=grid
article_dir=article-run
list=cutest-unc-free.list
#list=cutest-test2.list
optfile=../../cutest/cutest.opt
tol="1e-5"
kmax=5000
maxtime=3600

rm -rf $article_dir
mkdir -p $article_dir/$file-dir

for f in execs/*.sh
do
  ln -s $PWD/$f $article_dir/${f//execs\//}
done

cp execs/paramop{4,6}.txt $article_dir
cp lists/*.list $article_dir

cd $article_dir

date +"%Y-%m-%d_%H%M" > date

N=4
S=$((1000/N))

for alpha in $(seq 1 $N)
do
  alpha=$(awk -v a=$alpha -v S=$S 'BEGIN{print S*a}')

  for beta in $(seq 0 $N)
  do
    beta=$(awk -v b=$beta -v S=$S 'BEGIN{print S*b}')

    # Running nomad for the 4 parameters
    echo "Running $alpha $beta"
    awk -v a=$alpha -v b=$beta \
      'BEGIN{print "alpha", a/1000; \
      print "beta", b/1000}' > alpha-beta.txt

    sed "s/ALPHA/$alpha/g" paramop4.txt | sed "s/BETA/$beta/g" > paramop.txt
    echo " - Optimizing 4 parameters"
    ./nomad-run.sh paramop.txt ../.. > $file-dir/nomad-result-m-$alpha-$beta.out

    new_nomad_dir=$file-dir/nomad-m-$alpha-$beta
    mv "$(cat nomad.dir)" $new_nomad_dir
    echo $new_nomad_dir > nomad.dir

    # Need to run nomad again for the 6 parameters
    x=($alpha $beta $(cat $new_nomad_dir/nomad_sol))
    args=(ALPHA BETA ETA1 ETA2 SIGMA1 SIGMA2)
    cp -f paramop6.txt paramop.txt
    for i in $(seq 0 5)
    do
      sed -i "s/${args[$i]}/${x[$i]}/g" paramop.txt
    done
    echo " - Optimizing 6 parameters"
    ./nomad-run.sh paramop.txt ../.. > $file-dir/nomad-result-$alpha-$beta.out
    new_nomad_dir=$file-dir/nomad-$alpha-$beta
    mv "$(cat nomad.dir)" $new_nomad_dir
    echo $new_nomad_dir > nomad.dir

    # Need to run
    x=($(cat $new_nomad_dir/nomad_sol))
    args=(alpha beta eta1 eta2 sigma1 sigma2)
    scal=(1000 1000 1000 100 100 10)
    for i in $(seq 0 5)
    do
      echo | awk -v arg=${args[$i]} -v val=${x[$i]} -v s=${scal[$i]} \
        '{print arg, val/s}'
    done > $optfile
    cat >> $optfile << EOF
tol $tol
kmax $kmax
maxtime $maxtime
EOF
    a=$(echo | awk -v s=${x[0]} '{printf "%.2f\n", s/1000}')
    b=$(echo | awk -v s=${x[1]} '{printf "%.2f\n", s/1000}')
    echo " - Running complete list of problems"
    ./runlist.sh $list ../.. > log-$a-$b
    mv $(readlink last) result-$a-$b
    echo "$alpha $beta" > result-$a-$b/origin
  done
done

# Running for (1.00-0.00) and (1.00-1.00)
for sp in 1000-0 1000-1000
do
  x=(${sp//-/ } $(cat $file-dir/nomad-m-$sp/nomad_sol))
  args=(alpha beta eta1 eta2 sigma1 sigma2)
  scal=(1000 1000 1000 100 100 10)
  for i in $(seq 0 5)
  do
    echo | awk -v arg=${args[$i]} -v val=${x[$i]} -v s=${scal[$i]} \
      '{print arg, val/s}'
  done > $optfile
  cat >> $optfile << EOF
tol $tol
kmax $kmax
maxtime $maxtime
EOF
  a=$(echo | awk -v s=${x[0]} '{printf "%.2f\n", s/1000}')
  b=$(echo | awk -v s=${x[1]} '{printf "%.2f\n", s/1000}')
  echo "Running for special $sp"
  ./runlist.sh $list ../.. > log-$a-$b-sp
  mv $(readlink last) result-$a-$b-sp
done

rm -f *.sh *.list last

cd ..
