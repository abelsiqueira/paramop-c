using Plots
pyplot(size=(600,450), reuse=true)
using LaTeXStrings

# Plots using comp.data
function read_comp_data()
  lines = readlines("comp.data")[2:end]
  N = length(lines)
  rob = zeros(N)
  T = zeros(N)
  A = zeros(N)
  B = zeros(N)
  for (i,line) in enumerate(lines)
    sline = split(line)
    A[i], B[i] = map(parse, split(sline[1], "-")[2:3])/1000
    rob[i], T[i] = map(parse, sline[2:3])
  end
  return rob, T, A, B
end

function plots(rob, T, A, B)
  # Plot fastest for each rob
  robs = sort(unique(rob))
  Ifast = []
  Islow = []
  for r in robs
    Ir = find(rob .== r)
    tmin = minimum(T[Ir])
    It = find(T[Ir] .== tmin)
    if length(It) > 1
      error("Not prepared for this")
    end
    push!(Ifast, Ir[It][1])
    tmax = maximum(T[Ir])
    It = find(T[Ir] .== tmax)
    if length(It) > 1
      error("Not prepared for this")
    end
    push!(Islow, Ir[It][1])
  end

  Irob = find(rob .== robs[end])
  sort!(Irob, by=i->T[i])

  plot()
  plot!(rob[Ifast], T[Ifast], l=:dash, c=:red, m=(0,:blue), leg=false)
  plot!(rob[Islow], T[Islow], l=:dot, c=:magenta, m=(0,:blue), leg=false)
  plot!(rob[Irob], T[Irob], l=:solid, c=:blue, m=(0,:blue), leg=false)
  scatter!(rob, T, leg=false, m=(3,:blue,stroke(0)))
  i = findfirst((A .== 1.0) & (B .== 0.0))
  scatter!(rob[i:i], T[i:i], m=(15,:xcross,:red))
  i = findfirst((A .== 1.0) & (B .== 1.0))
  scatter!(rob[i:i], T[i:i], m=(15,:black,:xcross))
  annotate!(rob[Ifast[1:1]], T[Ifast[1:1]]-0.01, text("Worst",8))
  annotate!(rob[Ifast[end:end]], T[Ifast[end:end]]-0.01, text("Best",8))
  xlims!(minimum(rob)-1, maximum(rob)+1)
  ylims!(0, maximum(T)*1.05)
  xlabel!("Robustness")
  ylabel!("Elapsed time for converged problems")
  png("rob-time")

  plot(A[Ifast], B[Ifast], l=:dash, c=:red, m=(4,:red), leg=false)
  plot!(A[Islow], B[Islow], l=:dot, c=:magenta, m=(4,:magenta), leg=false)
  plot!(A[Irob], B[Irob], l=:solid, c=:blue, m=(4,:blue), leg=false)
  annotate!(A[Ifast[1:1]]-0.04, B[Ifast[1:1]], text("Worst",8))
  annotate!(A[Ifast[end:end]], B[Ifast[end:end]]-0.03, text("Best",8))
  i = findfirst((A .== 1.0) & (B .== 0.0))
  scatter!(A[i:i], B[i:i], m=(15,:xcross,:red))
  i = findfirst((A .== 1.0) & (B .== 1.0))
  scatter!(A[i:i], B[i:i], m=(15,:black,:xcross))
  xlims!(-0.05, 1.05)
  ylims!(-0.05, 1.05)
  xlabel!(L"$\alpha$")
  ylabel!(L"$\beta$")
  png("rob-time-on01")
end

function info(rob, T, A, B)
  Imaxrob = find(rob .== maximum(rob))
  println(Imaxrob)
  best = minimum(T[Imaxrob])
  println("Best time of maximum robustness: $best")
  I = find(T[Imaxrob] .== best)
  println(I)
  bests = Imaxrob[I]
  for i in bests
    println("$(A[i]),$(B[i]): $(rob[i]), $(T[i])")
  end
end

function runall()
  rob, T, A, B = read_comp_data()
  plots(rob, T, A, B)
  info(rob, T, A, B)
end

runall()
