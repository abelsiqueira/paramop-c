using Plots

pyplot(size=(600,600*3/4), reuse=true, grid=true)
using LaTeXStrings

function dominated(F, t, k, bot_is_best = false)
  for (tf, kf, name) in F
    if tf == t && kf == k
      continue
    end
    if bot_is_best
      if tf <= t && kf <= k
        return true
      end
    else
      if tf >= t && kf >= k
        return true
      end
    end
  end
  return false
end

function isin(F, x, y)
  for (xf, yf, name) in F
    if (xf,yf) == (x,y)
      return true
    end
  end
  return false
end

function update!(F, t, k, name, bot_is_best = false)
  if dominated(F, t, k, bot_is_best)
    return
  end
  push!(F, (t, k, name))
  for i = length(F):-1:1
    if dominated(F, F[i][1], F[i][2], bot_is_best)
      deleteat!(F, i)
    end
  end
end

function parse_lines(lines)
  T = Float64[]
  K = Float64[]
  names = ASCIIString[]

  for line in lines
    sline = split(line)
    push!(T, parse(sline[1]))
    push!(K, parse(sline[2]))
    push!(names, sline[3])
  end
  return T, K, names
end

function gen_plot(X, Y, names, algname, xlabel, ylabel; targets = [],
  bot_is_best = true)

  tgt_points = Dict()

  F = []
  N = length(X)
  A = zeros(N)
  B = zeros(N)

  for i = 1:N
    x, y, name = X[i], Y[i], names[i]
    A[i], B[i] = map(parse, split(names[i],"-"))/1000
    if name in targets
      tgt_points[name] = [x, y]
    end
    update!(F, x, y, name, bot_is_best)
  end

  println("Best of $algname:")
  for (x, y, name) in F
    println("$name: $x, $y")
  end

  p = plot()

  xmin, xmax = extrema(X)
  ymin, ymax = extrema(Y)
  σ = [xmax-xmin, ymax-ymin]*0.05
  xmin, xmax = [xmin;xmax] + [-1;1]*σ[1]
  ymin, ymax = [ymin;ymax] + [-1;1]*σ[2]
  styles = [:solid, :dash]
  for (i,tgt) in enumerate(targets)
    x, y = tgt_points[tgt]
    plot!([x;x], [ymin,ymax], leg=false, c=:black,
      l=styles[i])
    plot!([xmin,xmax], [y;y], leg=false, c=:black,
      l=styles[i])
  end

  scatter!(X, Y, c=:blue, m=(3,stroke(0)), leg=false)

  xlabel!(xlabel)
  ylabel!(ylabel)
  xlims!(xmin, xmax)
  ylims!(ymin, ymax)

  png(algname)

  # New plot
  scatter(X, Y, c=:blue, m=(3,stroke(0)), leg=false)
  xlabel!(xlabel)
  ylabel!(ylabel)
  xmin, xmax = extrema([v[1] for v in F])
  ymin, ymax = extrema([v[2] for v in F])
  σ = [xmax-xmin, ymax-ymin]*0.12
  xlims!(xmin-σ[1], xmax+σ[1])
  ylims!(ymin-σ[2], ymax+σ[2])
  png("$algname-zoom")

  # New plot
  scatter(X, Y, c=:blue, m=(2,stroke(0)), leg=false)
  xlabel!(xlabel)
  ylabel!(ylabel)
  xlims!(xmin-σ[1], xmax+σ[1])
  ylims!(ymin-σ[2], ymax+σ[2])
  i = 0
  sort!(F, by=v->v[1])
  σ = 0.05
  for (x,y,name) in F
    i += 1
    scatter!([x], [y], ann=(x-3, y, text("($i)",8)), m=(4,:red,stroke(0)))
  end
  png("$algname-filter")

  # New plot
  plot()
  i = 0
  for (x,y,name) in F
    α, β = map(parse, split(name,"-"))/1000
    i += 1
    plot!([α;0.8], [β;0.8-0.08i], line=(1,:gray))
    scatter!([α], [β], m=(4,:red,stroke(0)), ann=(0.83, 0.8-0.08*i,
      text("($i)",8)), leg=false)
    println("$i -> $α,$β")
  end
  xlims!(0, 1)
  ylims!(0, 1)
  xlabel!(L"$\alpha$")
  ylabel!(L"$\beta$")
  png("$algname-filter-on01")

  diagline = Any[]
  for (k,name) in enumerate(names)
    α, β = map(parse, split(name,"-"))
    if α + β != 1000
      continue
    end
    α, β = [α;β]/1000
    push!(diagline, [α,β,X[k],Y[k]])
  end
  sort!(diagline, by=v->v[1])
  scatter(X, Y, c=:blue, m=(2,stroke(0)), leg=false)
  plot!([v[3] for v in diagline], [v[4] for v in diagline], leg=false,
    m=(2,:red,stroke(0)))
  xmin, xmax = extrema(X)
  ymin, ymax = extrema(Y)
  σ = [xmax-xmin, ymax-ymin]*0.05
  xmin, xmax = [xmin;xmax] + [-1;1]*σ[1]
  ymin, ymax = [ymin;ymax] + [-1;1]*σ[2]
  xlims!(xmin, xmax)
  ylims!(ymin, ymax)
  png("$algname-diag")

  # Good and bad points
  τbx, τby, τgx, τgy = 2, 3, 1.11, 1.8
  if bot_is_best
    bestx, besty = minimum(X), minimum(Y)
    Ibad = find((X .> bestx*τbx) | (Y .> besty*τby))
    Igood = find((X .<= bestx*τgx) & (Y .<= besty*τgy))
  else
    bestx, besty = maximum(X), maximum(Y)
    Ibad = find((X .< bestx*τbx) | (Y .< besty*τby))
    Igood = find((X .>= bestx*τgx) & (Y .>= besty*τgy))
  end

  #scatter(X, Y, c=:gray, m=(1,stroke(0)), leg=false)
  scatter(X[Igood], Y[Igood], c=:blue, m=(3,stroke(0)), leg=false)
  scatter!(X[Ibad], Y[Ibad], c=:red, m=(4,stroke(1),:xcross), leg=false)
  xlims!(xmin, xmax)
  ylims!(ymin, ymax)
  xlabel!(xlabel)
  ylabel!(ylabel)
  png("$algname-good-bad")

  scatter(A[Igood], B[Igood], c=:blue, m=(3,stroke(0)), leg=false)
  scatter!(A[Ibad], B[Ibad], c=:red, m=(4,stroke(1),:xcross), leg=false)
  xlims!(-0.05, 1.05)
  ylims!(-0.05, 1.05)
  xlabel!(L"$\alpha$")
  ylabel!(L"$\beta$")
  png("$algname-good-bad-on01")

  println("Good are $(round(100*length(Igood)/N,1))% of total")
  println("Bad are $(round(100*length(Ibad)/N,1))% of total")
  for tgt in targets
    k = findfirst(tgt .== names)
    println("$(names[k]): $(X[k]/bestx), $(Y[k]/besty)")
  end

  return F
end

function prof_on01(F)
  plot(leg=false)
  for (x, y, name) in F
    α, β = map(parse, split(name,"-"))/1000
    color = x < 95 ? :red : :blue
    scatter!([α], [β], m=(4,color,stroke(0)))
  end
  xlims!(0, 1)
  ylims!(0, 1)
  xlabel!(L"$\alpha$")
  ylabel!(L"$\beta$")
  png("prof-data-filter-on01")
end

lines = readlines(open("time-iter.data"))
T, K, names = parse_lines(lines)
gen_plot(K, T, names, "time-iter", "Iterations", "Time (s)",
  targets = ["1000-0", "1000-1000"])

lines = readlines(open("prof.data"))
rob, eff, names = parse_lines(lines)
F = gen_plot(rob, eff, names, "prof-data", "Robustness", "Efficiency",
  bot_is_best = false)

prof_on01(F)
