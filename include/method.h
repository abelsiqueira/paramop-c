/* Copyright Abel Soares Siqueira - 2015
 * Licensed under GNU GPL v3
 *
 * Trust Region Method
 */

#include "helper_functions.h"
#include "solution.h"
#include "options.h"

// fun(x, f) -> cfn
typedef void (*Fun) (double *, double *);
// grad(x, g)
typedef void (*Grad) (double *, double *);
// hprod(x, v, Hv)
typedef void (*Hprod) (double *, double *, double *);

void step(double *, int, double *, double *, Hprod, double = 1e-5, double = 100);
void solve(Fun, Grad, Hprod, Solution *, Options *);
