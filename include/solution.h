/* Copyright (C) 2015  Abel Soares Siqueira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Solution keeps x, gx, k and ef.
typedef struct _Solution {
  int nvar;
  double *x;
  double fx;
  double *gx;
  int k;
  int exitflag;
  double el_time;
  int nf;
  int ng;
} Solution;

Solution * createSolution(int);
void destroySolution(Solution *);
