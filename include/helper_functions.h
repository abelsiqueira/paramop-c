/* Copyright Abel Soares Siqueira - 2015
 * Licensed under GNU GPL v3
 *
 * Helper functions
 */

#include <cmath>
#include <time.h>
#include <cstdio>

void copy(double *dest, double *src, int nvar);
void copy(double *dest, double *src, int nvar, double a);
double norm(int nvar, double *x);
double dot(int nvar, double *x, double *y);
// z <- x + y
void zexpy(double *z, double *x, double *y, int nvar);
// z <- a*x + y
void zeaxpy(double *z, double a, double *x, double *y, int nvar);
// x <- x + y
void xpy(double *x, double *y, int nvar);
// x <- a*x + y
void axpy(double *x, double a, double *y, int nvar);
// x <- x + b*y
void xpby(double *x, double b, double *y, int nvar);
// x <- a*x + b*y
void axpby(double *x, double a, double b, double *y, int nvar);
double getTime();

double maximum(int nvar, double *x);
void printvector(int, double *, const char *);
