/* Copyright (C) 2015  Abel Soares Siqueira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <sstream>
#include <fstream>

#define DEFAULT_ALPHA 1.0
#define DEFAULT_BETA 0.0
#define DEFAULT_ETA1 0.25
#define DEFAULT_ETA2 0.75
#define DEFAULT_INIT_CONV_MONO 0.85
#define DEFAULT_KMAX 1000
#define DEFAULT_MAXTIME 600
#define DEFAULT_SIGMA1 0.1666666667
#define DEFAULT_SIGMA2 4.0
#define DEFAULT_TOL 1e-8

typedef struct _Options {
 double tol;
 int kmax;
 double eta1; // Step acceptance and radius decrease
 double eta2; // Radius increase
 // Delta is updated according to rule Delta = mu^alpha * normg^beta
 double alpha;
 double beta;
 double sigma1; // Radius decrease ratio
 double sigma2; // Radius increase ratio
 double maxtime;
 double init_conv_mono;
} Options;

Options * createOptions(double = DEFAULT_TOL, int = DEFAULT_KMAX,
    double = DEFAULT_ETA1, double = DEFAULT_ETA2, double = DEFAULT_ALPHA,
    double = DEFAULT_BETA, double = DEFAULT_SIGMA1, double = DEFAULT_SIGMA2,
    double = DEFAULT_MAXTIME, double = DEFAULT_INIT_CONV_MONO);
Options * createOptions(const char * string);
void destroyOptions(Options *);
void printOptions(Options *);
