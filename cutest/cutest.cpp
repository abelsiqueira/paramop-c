#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdio>
#include "method.h"
extern "C" {
#include "cutest.h"
}

using namespace std;

int nvar = 0;
int status = 0;
bool goth[1] = {0};

void fun(double *x, double *f) {
  CUTEST_ufn(&status, &nvar, x, f);
}

void grad(double *x, double *g) {
  CUTEST_ugr(&status, &nvar, x, g);
}

void hprod(double *x, double *v, double *Hv) {
  CUTEST_uhprod(&status, &nvar, goth, x, v, Hv);
}

int MAINENTRY() {
  double *x0, *bl, *bu;
  int ncon = 0;
  char fname[10] = "OUTSDIF.d";
  int funit = 42, ierr = 0, fout = 6, io_buffer = 11;

  FORTRAN_open(&funit, fname, &ierr);
  CUTEST_cdimen(&status, &funit, &nvar, &ncon);

  if (ncon > 0) {
    cout << "ERROR: Constrained problem" << endl;
    return 1;
  }

  Solution *sol = createSolution(nvar);
  x0 = new double[nvar];
  bl = new double[nvar];
  bu = new double[nvar];

  CUTEST_usetup(&status, &funit, &fout, &io_buffer, &nvar, x0, bl, bu);
  for (int i = 0; i < nvar; i++) {
    if (bl[i] > -1e20 || bu[i] < 1e20) {
      cout << "ERROR: Bounded problem" << endl;
      return 1;
    }
    sol->x[i] = x0[i];
  }

  Options *opt = createOptions("cutest.opt");
  printOptions(opt);

  // Let's run once to get the exitflag and the expected time.
  solve(fun, grad, hprod, sol, opt);
  double el_time = sol->el_time;
#ifndef DEBUG
  // We consider a fail as a penalty to the objective function,
  // i.e., we return the maxtime.
  double best_o_time = 1e20, best_el_time = 1e20;
  if (sol->exitflag != 0) {
    cout << "Failure, original time was " << el_time << endl;
    best_o_time = opt->maxtime;
    best_el_time = opt->maxtime;
  } else {
    // We'll run the method enough times so that the expected time
    // falls between 0.1 and 1.0 seconds.
    int N = 1;
    if (el_time < 0.1)
      N = ceil(0.1/el_time);
    el_time = 0.0;
    printf("Running problem %d times, best of 3\n", N);

    for (int t = 0; t < 3; t++) {
      double o_time = getTime();
      for (int i = 0; i < N; i++) {
        copy(sol->x, x0, nvar);
        sol->k = 0;
        sol->nf = 0;
        sol->ng = 0;
        solve(fun, grad, hprod, sol, opt);
        el_time += sol->el_time;
      }
      o_time = (getTime() - o_time)/N;
      el_time /= N;
      if (el_time < best_el_time) {
        best_el_time = el_time;
        best_o_time = o_time;
      }
    }
  }
#endif

  if (nvar < 5) {
    printf("x =");
    for (int i = 0; i < nvar; i++)
      printf(" %15.8e", sol->x[i]);
    printf("\n");
  }
  printf("fx = %15.8e\n", sol->fx);
  printf("|gx| = %15.8e\n", norm(nvar, sol->gx));
  cout << "k = " << sol->k << endl;
  cout << "nf = " << sol->nf << endl;
  cout << "ng = " << sol->ng << endl;
  cout << "exit flag = " << sol->exitflag << endl;
  cout << "elapsed time = " << best_el_time << endl;
  cout << "o_time = " << best_o_time << endl;

  destroySolution(sol);
  destroyOptions(opt);
  delete []bl;
  delete []bu;

  FORTRAN_close(&funit, &ierr);

  return 0;
}
