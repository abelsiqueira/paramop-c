all: lib test

cutest: lib
	$(MAKE) -C cutest

.PHONY: lib test cutest
lib test:
	$(MAKE) -C $@

clean purge:
	$(MAKE) -C lib $@
	$(MAKE) -C test $@
	$(MAKE) -C cutest $@
