#include <iostream>
#include "method.h"

using namespace std;

void fun(double *x, double *f) {
  *f = pow(x[0] - 1.2, 4) + pow(x[1] + 0.5, 4);
}

void grad(double *x, double *g) {
  g[0] = 4*pow(x[0] - 1.2, 3);
  g[1] = 4*pow(x[1] + 0.5, 3);
}

void hprod(double *x, double *v, double *Hv) {
  Hv[0] = 12*pow(x[0] - 1.2, 2)*v[0];
  Hv[1] = 12*pow(x[1] + 0.5, 2)*v[1];
}

int main() {
  Solution *sol;
  sol = createSolution(2);
  sol->x[0] = 0.2;
  sol->x[1] = 0.3;
  Options *opt;
  opt = createOptions();

  solve(fun, grad, hprod, sol, opt);

  cout << "x = " << sol->x[0] << " " << sol->x[1] << endl;
  cout << "|g| = " << norm(2, sol->gx) << endl;
  cout << "k = " << sol->k << endl;
  cout << "nf = " << sol->nf << endl;
  cout << "ng = " << sol->ng << endl;
  cout << "exit flag = " << sol->exitflag << endl;

  destroySolution(sol);
  destroyOptions(opt);

  return 0;
}
