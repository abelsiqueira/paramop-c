#include <iostream>
#include <cstdio>
#include "method.h"

using namespace std;

void fun(double *x, double *f) {
  *f = 100*pow(x[1] - x[0]*x[0],2) + pow(1 - x[0], 2);
}

void grad(double *x, double *g) {
  double a = x[1]-x[0]*x[0];
  g[0] = -400*x[0]*a - 2*(1-x[0]);
  g[1] = 200*a;
}

void hprod(double *x, double *v, double *Hv) {
  Hv[0] = (1200*x[0]*x[0] - 400*x[1] + 2)*v[0] - 400*x[0]*v[1];
  Hv[1] = -400*x[0]*v[0] + 200*v[1];
}

int main() {
  Solution *sol = createSolution(2);
  sol->x[0] = -1.2;
  sol->x[1] = 0.8;
  Options *opt = createOptions();

  solve(fun, grad, hprod, sol, opt);

  printf("x = %10.6e %10.6e\n", sol->x[0], sol->x[1]);
  cout << "|g| = " << norm(2, sol->gx) << endl;
  cout << "k = " << sol->k << endl;
  cout << "nf = " << sol->nf << endl;
  cout << "ng = " << sol->ng << endl;
  cout << "exit flag = " << sol->exitflag << endl;

  destroySolution(sol);
  destroyOptions(opt);

  return 0;
}
