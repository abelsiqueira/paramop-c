/* Copyright Abel Soares Siqueira - 2015
 * Licensed under GNU GPL v3
 *
 * Trust Region Method
 */

#include <iostream>
#include "method.h"

void step(double *z, int nvar, double *x, double *g, Hprod hprod, double Delta,
    double tol, int kmax) {
  for (int i = 0; i < nvar; i++)
    z[i] = 0.0;
  double zp[nvar];
  double r[nvar];
  double d[nvar];
  double Bd[nvar];
  copy(r, g, nvar);
  double dot_rt = dot(nvar, r, r);
  double normg = norm(nvar, g);

  if (norm(nvar, r) < tol)
    return;

  copy(d, r, nvar, -1.0);

  int k = 0;
  kmax *= nvar;

  while (k < kmax) {
    hprod(x, d, Bd);
    double dtBd = dot(nvar, d,Bd), dtd = dot(nvar, d,d);
    if (dtBd <= 1e-12*dtd) {
      // Find tau such that p = z + tau*d minimizes m(p) and |p| = Delta
      double dtz = dot(nvar, d,z), ztz = dot(nvar, z,z);
      double D = sqrt(dtz*dtz - dtd*(ztz-Delta*Delta));
      double tau = (-dtz + D)/dtd;
      xpby(z, tau, d, nvar);
      return;
    }
    double alpha = dot_rt/dtBd;
    zeaxpy(zp, alpha, d, z, nvar);
    if (dot(nvar, zp,zp) > Delta*Delta) {
      double dtz = dot(nvar, d,z), ztz = dot(nvar, z,z);
      double D = sqrt(dtz*dtz - dtd*(ztz-Delta*Delta));
      double tau = (-dtz + D)/dtd;
      xpby(z, tau, d, nvar);
      return;
    }
    copy(z, zp, nvar);
    xpby(r, alpha, Bd, nvar);
    double dot_rtp = dot(nvar, r, r);
    if (norm(nvar, r)/normg < tol)
      return;
    double beta = dot_rtp/dot_rt;
    axpby(d, beta, -1.0, r, nvar);
    dot_rt = dot_rtp;
    k += 1;
  }

  return;
}

void solve(Fun fun, Grad grad, Hprod hprod, Solution *sol, Options *opt) {
  double alpha = opt->alpha;
  double beta = opt->beta;
  double eta1 = opt->eta1;
  double eta2 = opt->eta2;
  int kmax = opt->kmax;
  double sigma1 = opt->sigma1;
  double sigma2 = opt->sigma2;
  double tol = opt->tol;

  int nvar = sol->nvar;
  double *x = sol->x;
  double *gx = sol->gx;
  double *fx = &(sol->fx), fxp;
  double xp[nvar];
  copy(xp, x, nvar);
  sol->exitflag = 0;
  double start_time = getTime();

  FILE *file = fopen("fileiter.dat", "w");

  /*
  double conv1 = opt->init_conv_mono;
  double conv2 = 0.0;
  */

  /*
  int mem_size = 5;
  double oldfs[mem_size];
  for (int i = 0; i < mem_size; i++)
    oldfs[i] = -1e20;
    */

  fun(x, fx);
  sol->nf++;
  //oldfs[0] = *fx;
  grad(x, gx);
  sol->ng++;

  double normg = norm(nvar, gx);
  double d[nvar], Bd[nvar];

  double Delta, mu = 100;

  Delta = pow(mu,alpha)*pow(normg,beta);

  fprintf(file, "%d %e\n", 0, normg);

  int *k = &(sol->k);
  double *el_time = &(sol->el_time);
  *el_time = getTime() - start_time;

  while (normg > tol) {
    step(d, nvar, x, gx, hprod, Delta, tol, 100);
#ifdef DEBUG
    if (nvar < 4) {
      printvector(nvar, x, "x");
      printvector(nvar, gx, "gx");
      printvector(nvar, d, "d");
    } else {
      printf("|x| = %15.8e\n", norm(nvar, x));
      printf("|gx| = %15.8e\n", norm(nvar, gx));
      printf("|d| = %15.8e\n", norm(nvar, d));
    }
#endif
    hprod(x, d, Bd);
    zexpy(xp, x, d, nvar);
    fun(xp, &fxp);
    sol->nf++;
#ifdef DEBUG
    printf("fxp = %15.8e\n", fxp);
#endif

    /*
    double temp = conv1;
    conv1 = (conv1 + conv2)/2;
    conv2 = temp;
    */

    //double Ared = maximum(mem_size, oldfs)*conv2 + (1-conv2)*(*fx) - fxp;
    double Ared = *fx - fxp;
    double Pred = -dot(nvar, gx, d) - 0.5*dot(nvar, d, Bd);

#ifdef DEBUG
    printf("Ared = %15.8e\n", Ared);
    printf("Pred = %15.8e\n", Pred);
#endif
    if (Pred <= 0.0) {
      sol->exitflag = 3;
      break;
    }
    if (Ared > eta1*Pred) {
      copy(x, xp, nvar);
      *fx = fxp;
      grad(x, gx);
      sol->ng++;
#ifdef DEBUG
      if (nvar < 4) {
        printvector(nvar, gx, "gx");
      }
#endif
      normg = norm(nvar, gx);
      if (isnan(normg)) {
        sol->exitflag = -2;
        break;
      }
      if (Ared > eta2*Pred) {
        if (norm(nvar, d) > Delta/2.0)
          mu *= sigma2;
      }
    } else {
      mu *= sigma1;
    }
    Delta = pow(mu,alpha)*pow(normg,beta);
    (*k)++;
    fprintf(file,  "%d %e\n", *k, normg);
    if (*k >= kmax) {
      sol->exitflag = 1;
      break;
    }
    //oldfs[(*k)%mem_size] = *fx;
    *el_time = getTime() - start_time;
    if (*el_time > opt->maxtime) {
      sol->exitflag = 2;
      break;
    }
  }

  fclose(file);

  return;
}
