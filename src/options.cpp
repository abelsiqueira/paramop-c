/* Copyright (C) 2015  Abel Soares Siqueira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"

using namespace std;

Options * createOptions(double tol, int kmax, double eta1, double eta2,
    double alpha, double beta, double sigma1, double sigma2, double maxtime,
    double init_conv_mono) {
  Options * opt = new Options;
  opt->tol = tol;
  opt->kmax = kmax;
  opt->eta1 = eta1;
  opt->eta2 = eta2;
  opt->alpha = alpha;
  opt->beta = beta;
  opt->sigma1 = sigma1;
  opt->sigma2 = sigma2;
  opt->maxtime = maxtime;
  opt->init_conv_mono = init_conv_mono;
  return opt;
}

Options * createOptions(const char * filename) {
  Options * opt = createOptions();
  ifstream opt_file(filename);
  if (!opt_file.fail()) {
    string param, value;
    while (getline(opt_file, param, ' ')) {
      getline(opt_file, value, '\n');
      stringstream aux;
      aux << value;
      if (param == "alpha") aux >> opt->alpha;
      else if (param == "beta") aux >> opt->beta;
      else if (param == "eta1") aux >> opt->eta1;
      else if (param == "eta2") aux >> opt->eta2;
      else if (param == "init_conv_mono") aux >> opt->init_conv_mono;
      else if (param == "kmax") aux >> opt->kmax;
      else if (param == "maxtime") aux >> opt->maxtime;
      else if (param == "sigma1") aux >> opt->sigma1;
      else if (param == "sigma2") aux >> opt->sigma2;
      else if (param == "tol") aux >> opt->tol;
      else
        printf("### %s not found\n", param.c_str());
    }
  }
  return opt;
}

void destroyOptions(Options *opt) {
  delete opt;
}

void printOptions(Options *opt) {
  printf("alpha = %lf\n", opt->alpha);
  printf("beta = %lf\n", opt->beta);
  printf("eta1 = %lf\n", opt->eta1);
  printf("eta2 = %lf\n", opt->eta2);
  printf("kmax = %d\n", opt->kmax);
  printf("init_conv_mono = %lf\n", opt->init_conv_mono);
  printf("maxtime = %lf\n", opt->maxtime);
  printf("sigma1 = %lf\n", opt->sigma1);
  printf("sigma2 = %lf\n", opt->sigma2);
  printf("tol = %lf\n", opt->tol);
}
