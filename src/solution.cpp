/* Copyright (C) 2015  Abel Soares Siqueira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solution.h"

Solution * createSolution(int nvar) {
  Solution *s = new Solution;
  s->nvar = nvar;
  s->x = new double[nvar];
  s->fx = 1e20;
  s->gx = new double[nvar];
  s->k = 0;
  s->exitflag = -1;
  s->el_time = 0.0;
  s->nf = 0;
  s->ng = 0;
  return s;
}

void destroySolution(Solution *s) {
  delete [](s->x);
  delete [](s->gx);
  delete s;
}
