/* Copyright Abel Soares Siqueira - 2015
 * Licensed under GNU GPL v3
 *
 * Helper functions
 */

#include "helper_functions.h"

void copy(double *dest, double *src, int nvar) {
  for (int i = 0; i < nvar; i++)
    dest[i] = src[i];
}

void copy(double *dest, double *src, int nvar, double a) {
  for (int i = 0; i < nvar; i++)
    dest[i] = a*src[i];
}

double norm(int nvar, double *x) {
  double s = 0.0;
  for (int i = 0; i < nvar; i++)
    s += x[i]*x[i];
  return sqrt(s);
}

double dot(int nvar, double *x, double *y) {
  double s = 0.0;
  for (int i = 0; i < nvar; i++)
    s += x[i]*y[i];
  return s;
}

// z <- x + y
void zexpy(double *z, double *x, double *y, int nvar) {
  for (int i = 0; i < nvar; i++)
    z[i] = x[i] + y[i];
}

// z <- a*x + y
void zeaxpy(double *z, double a, double *x, double *y, int nvar) {
  for (int i = 0; i < nvar; i++)
    z[i] = a*x[i] + y[i];
}

// x <- x + y
void xpy(double *x, double *y, int nvar) {
  for (int i = 0; i < nvar; i++)
    x[i] += y[i];
}

// x <- a*x + y
void axpy(double *x, double a, double *y, int nvar) {
  for (int i = 0; i < nvar; i++)
    x[i] = a*x[i] + y[i];
}

// x <- x + b*y
void xpby(double *x, double b, double *y, int nvar) {
  for (int i = 0; i < nvar; i++)
    x[i] += b*y[i];
}

// x <- a*x + b*y
void axpby(double *x, double a, double b, double *y, int nvar) {
  for (int i = 0; i < nvar; i++)
    x[i] = a*x[i] + b*y[i];
}

double getTime() {
  timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);
  return ((double) ts.tv_nsec)/((double) 1e9) + ((double) ts.tv_sec);
}

double maximum(int nvar, double *x) {
  double m = x[0];
  for (int i = 1; i < nvar; i++)
    if (x[i] > m) m = x[i];
  return m;
}

void printvector(int nvar, double *x, const char * xname) {
  printf("%s =", xname);
  for (int i = 0; i < nvar; i++)
    printf(" %15.8e", x[i]);
  printf("\n");
}
